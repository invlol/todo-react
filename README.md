#TODO React app

### Requirements

* Need nodejs and npm installed to run this project

## Installation

* Clone the repository
* Enter to the root project folder using your terminal
* Run ``` npm install ``` to download all dependencies
* Run ``` npm start ``` to run the application
* Go to http://localhost:3000 

Enjoy :)

## Architecture

One of the best choices to host an application is to use AWS platform.

To host our static react app on aws we can use an **S3 bucket**, **CloudFront**, **WAF** and **Route53**.

Rout53 is AWS DNS service, we can have our domin hosted and configure to use cloudfront "AWS CDN or content cache" service.
Cloudfront will reach our S3 bucket where our static website content will be stored. Cloudfront will cache our static content,
so every request will be faster than going directly to an S3 public website configuration.

If we need more sucurity we can include WAF that is a web app firewall to protect agains ddos attacks, SQL injections and more.

Here is an example how the architecture will look:

https://d2908q01vomqb2.cloudfront.net/22d200f8670dbdb3e253a90eee5098477c95c23d/2017/03/03/SMarck-finaldiagram.png