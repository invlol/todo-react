import React from 'react';
import { render } from 'react-dom'
import { Provider } from 'react-redux'

import { store, history } from './store'
import App from './components/App'

import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';

import './index.css'

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Switch>
        <Route path="/" component={App} />
      </Switch>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);