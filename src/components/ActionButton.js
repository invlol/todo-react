import React from 'react'
import PropTypes from 'prop-types'

const ActionButton =  ({ onClick, text }) => {
  return (
  <button
    onClick={ () => {onClick();} }
  >
    {text}
  </button>
  );
};

ActionButton.propTypes = {
  onClick: PropTypes.func.isRequired
};

export default ActionButton