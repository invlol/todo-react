import React from 'react'
import PropTypes from 'prop-types'
import ActionButton from './ActionButton';

const Todo = ({ todo, onToggle, removeTodo }) => (
  <li
    style={ {
      textDecoration: todo.completed ? 'line-through' : 'none'
    }}
  >
    {todo.text}
    <ActionButton text={todo.completed ? 'todo' : 'done'} onClick={() => onToggle(todo.id)} />
    <ActionButton text="remove" onClick={() => removeTodo(todo.id)} />
  </li>
);

Todo.propTypes = {
  todo: PropTypes.shape({
    id: PropTypes.number.isRequired,
    completed: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired
  }).isRequired,
  onToggle: PropTypes.func.isRequired
};

export default Todo