import React from 'react'
import PropTypes from 'prop-types'
import Todo from './Todo'

const TodoList = ({ todos, onToggle, removeTodo }) => (
  <ol>
    {todos.map((todo, index) => (
      <Todo key={todo.id}
            todo={todo}
            onToggle={onToggle}
            removeTodo={removeTodo} />
    ))}
  </ol>
);

TodoList.propTypes = {
  todos: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      completed: PropTypes.bool.isRequired,
      text: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  onToggle: PropTypes.func.isRequired
};

export default TodoList