import { createStore, applyMiddleware } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import reducer from './reducer';

import createHistory from 'history/createBrowserHistory';

export const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const myRouterMiddleware = routerMiddleware(history);


const getMiddleware = () => {
  return applyMiddleware(myRouterMiddleware);
};

export const store = createStore(
  reducer,
  getMiddleware()
);